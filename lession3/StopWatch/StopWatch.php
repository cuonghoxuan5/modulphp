<?php
 class StopWatch
 {
    private $startTime = 0;
    private $endTime   = 0;
    // Phương thức khởi tạo không tham số khởi tạo startTime với thời gian hiện tại của hệ thống.
     function __construct()
     {
        $this->startTime = time();
     }   
    //  trả về giá trị  thuộc tính start
     function getStartTime ()
     {
         return $this->startTime ;
     }
    //  trả giá trị  thuộc tính end
     function getEndTime()
     {
         return $this->endTime ;
     }
    //  Phương thức start() để reset startTime về thời gian hiện tại của hệ thống
    // thiết lập giá trị thuộc tính start 
     // microtime(true): thiết lập tg hiện tại 
     function start()
     {
        $this->startTime = microtime(true);
     }
    //  Phương thức stop() để thiết đặt endTime về thời gian hiện tại của hệ thống.
    // thiết lập giá trị thuộc tính stop 
    // microtime(true): thiết lập tg hiện tại 
     function stop ()
     {
        $this->endTime = microtime(true);
     }
    //  Phương thức getElapsedTime() trả về thời gian đã trôi qua theo số milisecond giây
    // trả về giá trị thuộc tính  getElapsedTime() 
     function getElapsedTime() 
     {
        return $this->endTime - $this->startTime;
     }
 }
 
 $stopWatch = new StopWatch();
  echo "<pre>";
 print_r($stopWatch);
 echo "</pre>";
//  echo  $stopWatch->getStartTime ();
//  cho đồng hồ chạy
 $stopWatch->start();

 for($i=1 ; $i <= 100000 ;$i++)
 {
    echo $i;
 }
//  cho đồng hồ dừng 
$stopWatch->stop();
echo "<br>";
echo $stopWatch->getElapsedTime();

