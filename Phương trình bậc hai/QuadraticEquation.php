<?php
 class  Quadratic
 {
     public function __construct ($ts_a , $ts_b , $ts_c)
     {
         $this->So_a = $ts_a;
         $this->So_b = $ts_b;
         $this->So_c = $ts_c;
     }
     function getSo_a ()
     {
         return $this->So_a;
     }
     function getSo_b ()
     {
         return $this->So_b;
     }
     function getSo_c ()
     {
         return $this->So_c;
     }
     function getDiscriminant() 
     {
         return  ($this->So_b *$this->So_b) - 4*( $this->So_a * $this->So_c);
     }
     function  getRoot1() 
     {
        $delta= $this->getDiscriminant();
        return  ((- $this->So_b) + sqrt($delta)) / (2 * $this->So_a);
     }
     function  getRoot2() 
     {
        $delta= $this->getDiscriminant();
         return ((- $this->So_b) - sqrt($delta)) / (2 * $this->So_a);
     }
    function total()
    {
        $delta= $this->getDiscriminant();
        $root1 = $this-> getRoot1();
        $root2 = $this-> getRoot2();
       
        if ($delta < 0)
        {
            echo " phương trình vô nghiệm";
        }elseif($delta == 0)
        {
            echo "phương trình có 1 nghiệm =" . $root1;
        }else{
            echo "phương trình có 2 nghiệm " . $root1 . ' ' . $root2 ;
        }
    }  
 }
 $quadratic = new Quadratic(1,3,1);
    //  echo "<pre>";
    //  print_r($quadratic);
    //  echo "</pre>";
 echo $quadratic->getDiscriminant();
 echo "<br>";
 echo $quadratic->getRoot1();
 echo "<br>";
 echo $quadratic->getRoot2();
 echo "<br>";
 echo $quadratic->total();