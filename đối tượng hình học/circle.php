<?php
    namespace circle;
     use  shape\Shape;
//  tạo lớp con Circle của lớp cha Shape  
    class Circle extends Shape
    {
        public int $radius;
        public function __construct($name,$radius)
        {
            parent::__construct($name);
            $this->radius = $radius;
        }
        // tính diện tích 
        public function calculateArea()
        {
            return pi() * pow($this->radius, 2);
        }
        // tính chu vi 
        public function calculatePerimeter()
        {
            return pi() * $this->radius * 2;
        }
    }