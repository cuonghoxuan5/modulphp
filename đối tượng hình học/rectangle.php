<?php
    namespace rectangle;
    use  shape\Shape;
     // tạp lớp con Rectangle của lớp cha Cylinder
    class Rectangle extends Shape
    {
        public $width;
        public $height;
        public function __construct( $name ,  $width, $height)
        {
            parent::__construct($name);
            $this->width = $width;
            $this->height = $height;
        }
        // tính diện tích 
        public function calculateArea()
        {
            return $this->height * $this->width;
        }
        // tính chu vi 
        public function calculatePerimeter()
        {
            return ($this->height + $this->width) * 2;
        }
    }









