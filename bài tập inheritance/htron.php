<?php
    namespace hinh_tron;
    class Hinh_tron
    {
        public $radius;
        public $color;
        public function __construct($ts_radius ,$ts_color)
        {
            $this->radius = $ts_radius;
            $this->color  = $ts_color;
        }
        public function toString()
        {
            echo "Chu vi hình tròn :" . $hinh_tron->calculatePerimeter() . "<br> <hr>";
            echo "Diện tích hình tròn :" . $hinh_tron->calculateArea() . "<br> <hr>";
        }
        // trả về giá trị của bán kính 
        public function getRadius()
        {
            return $this->radius;
        }
        // trả về giá trị của màu sắc 
        public function getColor()
        {
            return $this->color;
        }
         // thiết lập phương thức của bán kính
        public function setRadius($ts_radius)
        {
            $this->radius = $ts_radius;
        }
          // thiết lập phương thức của màu sắc
        public function setColor($ts_color)
        {
            $this->color = $ts_color;
        }
        // tính chu vi 
        public function calculatePerimeter()
        {
            return $this->radius * $this->radius *  3.14 ; 
        }
        // tính diện tích 
        public function calculateArea()
        {
            return $this->radius * 2 * 3.14 ;
        }
    }
   