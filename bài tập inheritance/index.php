<?php
    namespace indexs;
    include_once "./htron.php";
    include_once "./htru.php";
    use hinh_tron\Hinh_tron;
    use hinh_tru\Hinh_tru;

    $hinh_tron = new Hinh_tron(7, "blue");
    echo "Chu vi hình tròn :" . $hinh_tron->calculatePerimeter() . "<br> <hr>";
    echo "Diện tích hình tròn :" . $hinh_tron->calculateArea() . "<br> <hr>";

    $hinh_tru = new Hinh_tru(7 , "blue" , 10);
    echo "Thể tích hình trụ :" . $hinh_tru->volumetric();
