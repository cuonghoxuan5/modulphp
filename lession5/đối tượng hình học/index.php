<?php
    include_once "./shape.php";
    include_once "./Circle.php";
    include_once "./cylinder.php";
    include_once "./rectangle.php";
    include_once './square.php';
    use shape\Shape;
    use circle\Circle;
    use cylinder\Cylinder;
    use rectangle\Rectangle;
    use square\Square;

    $circles = new Circle('Circle 01' ,3);
    echo 'Circle area :' . $circles->calculateArea(). '<br />';
    echo 'Circle perimeter: ' . $circles->calculatePerimeter() . '<br /><hr>';

    $cylinders = new Cylinder('Cylinder 01', 3, 4);
    echo 'Cylinder area: ' . $cylinders->calculateArea() . '<br />';
    echo 'Cylinder perimeter: ' . $cylinders->calculatePerimeter() . '<br /> <hr>';

    $rectangles = new Rectangle('Rectangle 01', 3, 4);
    echo 'Rectangle area: ' . $rectangles->calculateArea() . '<br />';
    echo 'Rectangle perimeter: ' . $rectangles->calculatePerimeter() . '<br/><hr>';

    $squares = new Square('Square 01', 5);
    echo 'Square area: ' . $squares->calculateArea() . '<br />';
    echo 'Square perimeter: ' . $squares->calculatePerimeter() . '<br />';