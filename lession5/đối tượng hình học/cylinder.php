<?php
    namespace cylinder;
    use circle\Circle;
    // tạp lớp con Cylinder của lớp cha circle
    class Cylinder extends Circle
    {
        public $height;
        public function __construct($name , $radius , $height)
        {
            parent::__construct($name,$radius);
            $this->height = $height;
        }
        // diện tích trả về 
        public function calculateArea()
        {
            return parent::calculateArea() * 2 + parent::calculatePerimeter() * $this->height;
        }
        // tính toán khối lượng
        public function calculateVolume()
        {
            return parent::calculateArea() * $this->height;
        }
    }