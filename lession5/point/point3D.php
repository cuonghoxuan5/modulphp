<?php
    namespace point3d;
    include_once "./point2D.php";
    use point2d\Point2D;
    class Point3D extends Point2D 
    {
        public float $z;
        public function __construct($ts_x, $ts_y ,$ts_z)
        {
            parent::__construct($ts_x, $ts_y);
            $this->z = $ts_z;
        }
        // trả về giá trị phương thức 
        public function getZ()
        {
            return $this->z;
        }
        public function setZ()
        {
            $this->z = $ts_z;
        }
        public function getXYZ()
        {
            return parent::getXY() . $this->z;
        }
        public function setXYZ($ts_x ,$ts_y,$ts_z)
        {
            parent::setXY($ts_x ,$ts_y) . $this->z = $ts_z;
        }
        public function toString()
        { 
            echo "<hr>X :" . $this->x . " - " . "Y :" . $this->y . " - " . "Z :" . $this->z;
        }
    }
    $Point2d = new Point2D (1,2 );
    $Point3d = new Point3D (3,4,5);


    $Point2d->toString();
    $Point3d->toString();
    // echo "<pre>";
    // print_r ($Point2d);
    // print_r ($Point3d);
    // die();