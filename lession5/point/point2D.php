<?php
    namespace point2d;
    class Point2D 
    {
        public float $x;
        public float $y;
        public function __construct( $ts_x, $ts_y)
        {
            $this->x = $ts_x;
            $this->y = $ts_y;
        }
        // trả về giá trị của x 
        public function getX()
        {
            return $this->x ;
        }
        // trả về giá trị của y 
        public function getY()
        {
            return $this->y ;
        }
        // thiết lập phương thức x 
        public function setX($ts_x)
        {
            $this->x = $ts_x;
        }
        // thiết lập phương thức y
        public function setY( $ts_y)
        {
            $this->yy = $ts_y;
        }
        // thiết lập phương thức xy 
        public function setXY($ts_x ,$ts_y)
        {
            
             $this->x = $ts_x . $this->y = $ts_y;
        }
        // trả về giá trị của xy 
        public function getXY():array
        {
            return array $this->x . $this->y;
        }
        public function toString()
        {          
           echo "X :" . $this->x . " - ". "Y :" . $this->y . "<br>" ;
        }
    }
