<?php
    class ArrayList
    {
        public $arraylist;
        public function __construct($arr = '')
        {
            if(is_array($arr))
            {
                $this->arrayList = $arr;
            }else {
                $this->arrayList = [];
            }
        }
        // thêm 1 phần tử vào ds 
        public function add($obj)
        {
            array_push($this->arrayList , $obj);
        }
        public function get($index)
        {
            if( $this->isInteger($index) && $index < $this->size())
            {
                return $this->arrayList[$index];
            } else{
                die("ERROR in arrayList.get");
            }
        }
        public function isInteger($toCheck) :bool
        {
            return preg_match("/^[0-9]+$/", $toCheck);
        }    
        public function size()
        {
            return count ($this->arrayList);
        }
    }