<?php
    namespace hinh_tru;
    include_once "./htron.php";
    use hinh_tron\Hinh_tron;
    class Hinh_tru extends Hinh_tron
    {
        public $heigh;
        public function __construct($ts_radius ,$ts_color, $ts_heigh)
        {
            parent::__construct($ts_radius ,$ts_color);
            $this->heigh = $ts_heigh;
        }
        public function toString()
        {

        }
        // ttinhs thể tích 
        public function volumetric()
        {
           return parent::calculateArea() * $this->heigh;
        }
    }
