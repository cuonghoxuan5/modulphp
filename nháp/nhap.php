<?php
  class Nhansu
  {
    public $ho      ='';
    public $ten     ='';
    public $ngaysinh='';
    public $diachi  ='';
    public $congviec='';
    function __construct($ho , $ten ,$ngaysinh ,$diachi ,$congviec)
    {
      $this->ho         = $ho;
      $this->ten        = $ten;
      $this->ngaysinh   = $ngaysinh;
      $this->diachi     = $diachi;
      $this->congviec   = $congviec;
    } 
    function getHo()
    {
      return $this->ho;
    }
    function setHo($ho)
    {
      $this->ho = $ho;
    }
    function getTen()
    {
      return $this->ten;
    }
    function setTen($ten)
    {
      $this->ten = $ten;
    }
    function getNgaysinh()
    {
      return $this->ngaysinh;
    }
    function setNgaysinh($ngaysinh)
    {
      $this->ngaysinh = $ngaysinh;
    }
    function getDiachi()
    {
      return $this->diachi = $diachi;
    }
    function setDiachi($diachi)
    {
      $this->diachi = $diachi;
    }
    function getCongviec()
    {
      return $this->$congviec;
    }
    function setCongviec($congviec)
    {
      $this->congviec = $congviec;
    }
  }
  $nhansu = [];
  $nhanvien1 = new Nhansu('hồ','xuân cường' , '03/08/2000' , 'gio linh','nhân viên');
  $nhanvien2 = new Nhansu('bùi', 'thị thu' , '25/02/2001', 'gio linh' ,'nhân viên');

  $nhansu[] = $nhanvien1;
  $nhansu[] = $nhanvien2;
  ?>

  <!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
  </head>
  <body>
    <table border = '1' width = 50%>
      <tr>
        <td> stt </td>
        <td> ho  </td>
        <td> ten </td>
        <td> ngay sinh </td>
        <td> dia chi </td>
        <td> cong viec </td>
      </tr>
      <tr>
        <?php foreach ($nhansu as $key =>$index):?>
      </tr>
      <tr>
        <td> <?php echo $key+1     ?> </td>
        <td> <?php echo $index->ho  ?> </td>
        <td> <?php echo $index->ten  ?></td>
        <td> <?php echo $index->ngaysinh  ?> </td>
        <td> <?php echo $index->diachi  ?> </td>
        <td> <?php echo $index->congviec  ?> </td>
      </tr>
      <?php endforeach ?>
  </body>
  </html>