<?php
    class User 
    {
        protected string $name = "";
        protected string $email= "";
        public int       $role;
        // Thuộc tính name có kiểu dữ liệu string thể hiện tên của người dùng, mức độ truy cập protected
        // trả về giá trị thuộc tính 
        protected function getName()
        {
            return $this->name;
        }
         //thiết lập giá trị của thuộc tính name 
        function setName( $ts_name)
        {
             $this->name = $ts_name;
        }
        // Thuộc tính email có kiểu dữ liệu string thể hiện email của người dùng, mức độ truy cập protected
        // thiết lập giá trị thuộc tính email 
        protected function getEmail()
        {
            return $this->email;
        }
        // trả về giá trị của thuộc tính role 
        public function getRole()
        {
            return $this->role;
        }
         // thiết lập thuộc tính role
        public function setRole($ts_role)
        {
            $this->role = $ts_role;
        }
        // trả về giá trị của thuộc tính email 
        function setEmail( $ts_email)
        {
             $this->email = $ts_email;
        }
        public function getInfo()
        {
            return $this->name ."<br>" . $this->email ."<br>". $this->role ."<br>";
        }
        /* Có phương thức isAdmin() kiểm tra nếu role bằng 1 hiển thị "là admin",
           nếu role bằng 2 hiển thị "là người dùng bình thường"*/
        public function isAdmin() 
        {
            if ($this->role == 1)
            {
                echo " admin ";
            }
            if ($this->role == 2)
            {
                echo " người dùng bình thường";
            }
        }
    }
    $user = new User();
    $user->setName("cường");
    $user->setEmail("cuong@gmail.com ");
    $user->setRole(2);
    echo "<br>" . $user->getInfo();
    echo "<br>" . $user->isAdmin();
    // echo "<pre>";
    // print_r ($user);
    // echo "</pre>";