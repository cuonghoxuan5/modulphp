<?php
   class Patient 
   {
       public $name;
       public $code;
       public $next;
   }
   class Queue 
   {
        public $front = null;
        public $back  = null;
        public function isEmpty()
        {
            return is_null($this->front);
        }
        public function enqueue($name, $code)
        {
            
                $dBack = $this->back;
               
                $this->back = new Patient();
                $this->back->name = $name;
                $this->back->code = $code;
                if($this->isEmpty())
            {
                $this->front = $this->back;
            }else{
                $dBack->next = $this->back;
            }
        }
        public function dequeue()
        {
            if($this->isEmpty())
            {
                return null;
            }
            $getName = $this->front->name;
            $getCode = $this->front->code;
            $this->front = $this->front->next;
            return $getName.$getCode; 
        }
   }

    $patient = new Queue();
$patient->enqueue("Nam", 3);
$patient->enqueue("Phuoc", 2);
$patient->enqueue("Khang", 6);
$patient->enqueue("Thanh", 5);
$patient->enqueue("Quan", 4);
$patient->enqueue("Phu", 2);
$patient->enqueue("Hao", 10);

//$patient->dequeue();

echo '<pre>';
print_r($patient);
echo '</pre>';