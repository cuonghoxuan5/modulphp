<?php
    include_once './Controller/CustomerController.php';

    $page = ( isset( $_GET['page'] ) ) ? $_GET['page'] : '';

    $objCustomerController = new CustomerController();

    switch ($page)
    {
        case 'index':
            $objCustomerController->index();
            break;
        case "create":
            $objCustomerController->create();
            break;
        case "edit":
            $objCustomerController->edit();
            break;
        case "delete":
            $objCustomerController->delete();
            break;
        default:
            # code...
            break;
    }
    echo "<pre>";
    print_r($_REQUEST);
    echo "</pre>";