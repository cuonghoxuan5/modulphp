<?php
  class Fan 
  {
    // 3 thuộc tính hằng được đặt tên là SLOW, MEDIUM, và FAST với giá trị 1, 2, và 3
     const SLOW   = 1;
     const MEDIUM = 2;
     const FAST   = 3;
    //  Thuộc tính speed có kiểu số nguyên và mức truy xuất là private để xác định tốc độ quạt, mặc định là SLOW
      private $Speed = "SLOW";
    //   Thuộc tính on có kiểu lô-gic và mức truy xuất private để xác định quạt đang bật hay tắt, mặc định là false (tắt).
      private $On    = false;
    //   Thuộc tính radius có kiểu thực và mức truy xuất private để xác định bán kính quạt, giá trị mặc định là 5
      private $Radius= 5;
    //   Thuộc tính color có kiểu dữ liệu là chuỗi và mức truy xuất private để xác định màu quạt, mặc định là "blue"
      private $Color = "blue";
    function __construct()
    {
       
    }
    //trả về giá trị phương thức speed 
    function getSpeed()
    {
        return $this->speed;
    }
    //thiết lập thuộc tính của speed 
    function setSpeed( $ts_speed)
    {
        $this->speed = $ts_speed;
    }
    // giá trị trả về phương thức on
    function getOn()
    {
        return $this->On;
    }
    // thiết lập thuộc tính của on
    function setOn($ts_on)
    {
        $this->on = $ts_on;
    }
    // trả về giá trị phương thức radius
    function getRadius()
    {
        $this->radius;
    }
    // thiết lập thuộc tính của radius
    function setRadius($ts_radius)
    {
        $this->radius = $ts_radius;
    }
    // trả về giá trị phương thức color
    function getColor()
    {
        return $this->color;
    }
    // thiết lập giá trị của color
    function setColor($ts_color)
    {
        $this->color = $ts_color;
    }
    // khởi tạo phương thức tostring 
    function toString()
    {
        if ($this->On == false)
        {
            echo "<br/>" . "Fan is on " . "<br/>" .  $this->Color . "<br/>" . $this->Radius . "<br/>"   . $this->Speed . "<br/>";
        }else{
            echo "<br/>" . "Fan is off " . "<br/>" . $this->Color . "<br/>"  . $this->Radius ;
        }
    }
  }
 // khởi tạo đóio tượng từ lớp fan 
  $fan = new Fan();
  echo $fan->toString();