<?php 
    include_once "DivideByZeroException.php";

   function Divide($a,$b)
   {
       if($b === 0 )
       {
           throw new DivideByZeroException();
       }
       return $a/$b;
   }
   try{
       $result = Divide(100,5);
       echo $result;
       $result = Divide(100,0);
       echo $result;
   }
   catch(DivideByZeroException $e)
   {
       echo " " . "xảy ra lỗi :" . $e;
   }