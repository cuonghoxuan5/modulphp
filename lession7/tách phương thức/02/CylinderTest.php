<?php
/**
 * Created by PhpStorm.
 * User: phanluan
 * Date: 26/10/2018
 * Time: 22:56
 */
require __DIR__ . "/../01/Cylinder.php";



class CylinderTest extends Cylinder
{
    /**
     *Test
     *DisplayName("Testing getVolume(0, 0)")
     */
    // public function __construct($radius , $height)
    // {
    //   parent::__construct($radius,$height);
    //   $this->radius = $radius;
    //   $this->height = $height;

    // }
    public function testGetVolume0And0()
    {
        $radius = 0;
        $height = 0;
        $expected = 0;

        $cylinder = new Cylinder();
        $result = $cylinder->getVolume($radius, $height);
        $this->assertEquals($expected, $result);
    }

    /**
     *Test
     *DisplayName("Testing getVolume(1, 2)")
     */

    public function testGetVolume1And2()
    {
        $radius = 1;
        $height = 2;
        $expected = 18.84955592153876;

        $cylinder = new Cylinder();
        $result = $cylinder->getVolume($radius, $height);
        $this->assertEquals($expected, $result);
    }
}



