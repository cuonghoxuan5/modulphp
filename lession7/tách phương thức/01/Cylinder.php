<?php

/**
 * Created by PhpStorm.
 * User: phanluan
 * Date: 26/10/2018
 * Time: 22:50
 */
class Cylinder 
{
    public function getVolume($radius, $height)
    {
       $baseArea  = $this->getArea($radius);
       $perimeter = $this->getMeter($radius);
        $volume = $perimeter * $height + 2 * $baseArea;
        return $volume;
    
    }
    public function getArea($radius)
    {
        $baseArea = pi() * $radius * $radius;
        return $baseArea;
    }
    public function getMeter($radius)
    {
        $perimeter = 2 * pi() * $radius;
        return $perimeter;
    }

}
$cylinder = new Cylinder();
echo " thể tích là :" . $cylinder->getVolume(4,6);


