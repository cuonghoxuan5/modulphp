<?php

class FizzBuzz
{
    public $status;

    public function __construct($number)
    {
        $Summary_variable01 = $number % 3 == 0;
        $Summary_variable02 = $number % 5 == 0;

        if ( $Summary_variable01 && $Summary_variable02) {
            $this->status = "FizzBuzz";
        } elseif ($Summary_variable01) {
            $this->status = "Fizz";
        } elseif ($Summary_variable02) {
            $this->status = "Buzz";
        } else {
            $this->status = $number . "";
        }
    }

    public function __toString()
    {
        return $this->status;
    }
}