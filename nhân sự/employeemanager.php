<?php
include_once './employee.php';

$employee =[];
$tran_a = new Employee('Trần', 'Văn A' ,'30/4/2001' ,'Gio linh' ,'Thu ngân');
$tran_b = new Employee('Trần', 'Văn B' ,'30/5/1996' ,'Đông hà' ,'Thu ngân');
$tran_c = new Employee('Trần', 'Văn C' ,'30/12/1999' ,'Đông hà' ,'Nhân viên');

 echo "<pre>";
 print_r ($tran_a);
 echo "</pre>";
//  Hiển thị danh sách nhân sự
 $employee[] = $tran_a;
 $employee[] = $tran_b;
 $employee[] = $tran_c;
 
//  Xem chi tiết thông tin nhân sự
echo "<pre>";
 print_r ($employee);
 echo "</pre>";
// xóa 1 nhân sự 
 unset($employee[1]);

 echo "<pre>";
 print_r ($employee);
 echo "</pre>";
//  Chỉnh sửa thông tin nhân sự
 $tran_a->setHo("Trần");
 $tran_a->setTen("Văn D");
 $tran_a->setBirth("19/3/1999");
 $tran_a->setAddress("vĩnh linh");
 $tran_a->setJob_position("nhân viên vân chuyển");
 
 echo "<pre>";
 print_r ($tran_a);
 echo "</pre>";
// $employees[1] = new Employee();
// $employees[1]->setHo('Trần');
// $employees[1]->setTen('Văn B');
// $employees[1]->setBirth('30/5/1997');
// $employees[1]->setAddress('Đông hà');
// $employees[1]->setJob_position('Thu ngân');

// echo "<pre>";
//  print_r ($employees);
//  echo "</pre>";
