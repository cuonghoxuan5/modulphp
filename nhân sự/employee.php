<?php
    class Employee 
    {
        public $Ho              = "";
        public $Ten             = "";
        public $Birth           = "";
        private $Address        = ""; 
        public $Job_position    = "";
        public function __construct($ts_ho,$ts_ten,$ts_birth ,$ts_address ,$ts_job_position ) 
        {
           
        $this->Ho            = $ts_ho;
        $this->Ten           = $ts_ten;
        $this->Birth         = $ts_birth;
        $this->Address       = $ts_address;
        $this->Job_position  = $ts_job_position;
        }
         // trả về giá trị phương thức họ
         public function getHo()
        {
            return $this->Ho ;
        }
         // thiết lập phương thức họ
        function setHo($ts_ho)
        {
            $this->Ho = $ts_ho;
        }
         // trả về giá trị phương thức tên     
         public function getTen()
        {
            return $this->Ten ;
        }
        // thiết lập phương thức tên
        function setTen($ts_ten)
        {
            $this->Ten = $ts_ten;
        }       
          // trả về giá trị phương thức ngày sinh
         public function getBirth()
        {
            return $this->Birth ;
        }
        // thiết lập phương thức ngày sinh
        function setBirth($ts_birth)
        {
            $this->Birth = $ts_birth;
        }
        // trả về giá trị phương thức địa chỉ  
        private function getAddress()
        {
            return $this->Address ;
        }
        // thiết lập phương thức ngày địa chỉ
        function setAddress($ts_address)
        {
            $this->Address = $ts_address;
        }
        // trả về giá trị phương thức ngày  vị trí công việc
        public function getJob_position()
        {
            return $this->Job_position;
        }
        // thiết lập giá trị phương thức vị trí công việc
        function setJob_position($ts_job_position)
        {
            $this->Job_position = $ts_job_position;
        }
    }
   