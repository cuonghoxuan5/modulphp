<?php
    class Stack 
    {
        private $data  = [];
        private $limit =0;
        public function __construct($limit)
        {
            $this->limit = $limit;
        }
        public function push($element)
        {
            array_push($this->data , $element);
        }
        public function pop()
        {
            if(!$this->isEmpty())
            {
                array_shift($this->data);
            }else{
                die("mảng rỗng");
            }
        }
        public function top()
        {
            return reset($this->data);
        }
        public function isEmpty()
        {
            if(count($this->data) == 0)
            {
                return true;
            }else{
                return false;
            }
        }
        public function isFull()
        {
            if(count ($this->data) == $this->limit)
            {
                return true;
            }else{
                return false;
            }
        }
    }
  
    $stack = new Stack(5);

    // $stack->push("1");
    // $stack->push("2");
    // $stack->push("3");
    // $stack->push("4");
    // $stack->push("5");

    // $stack->pop();
    // $stack->pop();
    // $stack->pop();

    // $stack->push("6");
    // $stack->push("7");

    // $stack->pop();
    // $stack->pop();
    // $stack->pop();
    // $stack->pop();
    // $stack->pop();

   echo  $stack->top();
    

    echo "<pre>";
    print_r($stack);
    echo "</pre>";
