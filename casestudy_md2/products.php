<?php
	include_once "admin/database.php";	
	include_once "layouts/header.php";
	
	$id = (isset($_REQUEST['id'])) ? $_REQUEST['id'] :"" ;

    $slq  = "SELECT * FROM products";
	$stmt = $connect->query($slq);
	$stmt->setFetchMode(PDO::FETCH_OBJ);
	$row  = $stmt->fetchAll();
    
	$sql 	= "SELECT * FROM products WHERE id = $id";
	echo $sql;
	die();
	$stmt1	= $connect->query($sql);
	$stmt1->setFetchMode(PDO::FETCH_OBJ);
	$rows 	= $stmt1->fetchAll();
	// echo "<pre>";
	// print_r($row);
	// die();


?>

<section class="main-content">

    <div class="row">
        <div class="span9">
            <ul class="thumbnails listing-products">
                <?php foreach($rows as $product): ?>
                <li class="span3">
                    <div class="product-box">
                        <span class="sale_tag"></span>
                        <a href="product_detail.php?id=<?= $product->id;?>"><img alt="" src="<?= $product->image;?>"
                                style="width:200px ; height:150px"></a><br />
                        <a href="product_detail.php?id=<?= $product->id;?>" class="title">Fusce id molestie
                            massa</a><br />
                        <a href="#" class="category">Phasellus consequat</a>
                        <p class="price">$341</p>
                    </div>
                </li>
                <?php endforeach; ?>
                <hr>
                <div class="pagination pagination-small pagination-centered">
                    <ul>
                        <li><a href="#">Prev</a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">Next</a></li>
                    </ul>
                </div>
        </div>
        <div class="span3 col">
            <div class="block">
                <?php foreach ($row as $product):?>
                <ul class="nav nav-list">

                    <li class="nav-header">SUB CATEGORIES</li>
                    <li><a href="products.php?id=<?= $product->id;?>">Nullam semper elementum</a></li>
                    <li class="active"><a href="products.php">Phasellus ultricies</a></li>
                    <li><a href="products.php">Donec laoreet dui</a></li>
                    <li><a href="products.php">Nullam semper elementum</a></li>
                    <li><a href="products.php">Phasellus ultricies</a></li>
                    <li><a href="products.php">Donec laoreet dui</a></li>
                </ul>
                <?php endforeach; ?>
                <br />
                <ul class="nav nav-list below">
                    <li class="nav-header">MANUFACTURES</li>
                    <li><a href="products.php"></a></li>
                    <li><a href="products.php">Lamboghini</a></li>
                    <li><a href="products.php">Dunlop</a></li>
                    <li><a href="products.php">Yamaha</a></li>
                </ul>
            </div>
            <div class="block">
                <h4 class="title">
                    <span class="pull-left"><span class="text">Randomize</span></span>
                    <span class="pull-right">
                        <a class="left button" href="#myCarousel" data-slide="prev"></a><a class="right button"
                            href="#myCarousel" data-slide="next"></a>
                    </span>
                </h4>
                <div id="myCarousel" class="carousel slide">
                    <div class="carousel-inner">
                        <div class="active item">
                            <ul class="thumbnails listing-products">
                                <li class="span3">
                                    <div class="product-box">
                                        <span class="sale_tag"></span>
                                        <img alt="" src="themes/images/ladies/01.jpg"><br />
                                        <a href="product_detail.php" class="title">Fusce id molestie massa</a><br />
                                        <a href="#" class="category">Suspendisse aliquet</a>
                                        <p class="price">$261</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="item">
                            <ul class="thumbnails listing-products">
                                <li class="span3">
                                    <div class="product-box">
                                        <img alt="" src="themes/images/ladies/02.jpg"><br />
                                        <a href="product_detail.php" class="title">Tempor sem sodales</a><br />
                                        <a href="#" class="category">Urna nec lectus mollis</a>
                                        <p class="price">$134</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block">
                <h4 class="title"><strong>Best</strong> Seller</h4>
                <ul class="small-product">
                    <li>
                        <a href="#" title="Praesent tempor sem sodales">
                            <img src="themes/images/ladies/03.jpg" alt="Praesent tempor sem sodales">
                        </a>
                        <a href="#">Praesent tempor sem</a>
                    </li>
                    <li>
                        <a href="#" title="Luctus quam ultrices rutrum">
                            <img src="themes/images/ladies/04.jpg" alt="Luctus quam ultrices rutrum">
                        </a>
                        <a href="#">Luctus quam ultrices rutrum</a>
                    </li>
                    <li>
                        <a href="#" title="Fusce id molestie massa">
                            <img src="themes/images/ladies/05.jpg" alt="Fusce id molestie massa">
                        </a>
                        <a href="#">Fusce id molestie massa</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<?php include "layouts/footer.php"; ?>