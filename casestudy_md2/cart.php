<?php 
// kiểm tra có tồn tại session k , lien kết vs addcart.php để lưu lại dữ liệu trong giở hàng
    session_start();
    if (!isset($_SESSION)) {
       
    }
	include_once "admin/database.php";
	

	$cart = (isset($_SESSION["cart"]) && !empty($_SESSION["cart"])) ? $_SESSION["cart"] : [];
    if(!$cart)
    {
        header("location:index.php");
    }
    $cart_ids   = implode(",",$cart);
    if (!$cart_ids) 
    {
        header("location:index.php");
    }
	$slq  = "SELECT * FROM products";
	$stmt = $connect->query($slq);
	$stmt->setFetchMode(PDO::FETCH_OBJ);
	$row  = $stmt->fetchAll();

	// $slq  = "SELECT * FROM oder_dentails";
	// $stmt = $connect->query($slq);
	// $stmt->setFetchMode(PDO::FETCH_OBJ);
	// $row  = $stmt->fetchAll();
    
    
    $sql2 = "SELECT * FROM products WHERE id IN (" . $cart_ids . ")";

    $stmt1 = $connect->query($sql2);
    // echo $stmt1;
    // die();
	$stmt1->setFetchMode(PDO::FETCH_OBJ);
	$row1  = $stmt1->fetchAll();
        // echo "<pre>";
        // print_r($row1->id);
        // die();
        include_once "layouts/header.php";
?>

<section class="main-content">
    <div class="row">
        <div class="span9">
            <h4 class="title"><span class="text"><strong>Your</strong> Cart</span></h4>
            <table class="table table-striped">
                <thead>
                    <tr>

                        <th>Image</th>
                        <th>Product Name</th>
                        <th>Quantity</th>
                        <th>Unit Price</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    $total_price = 0 ;
                    foreach ($row1 as $cart):
                    $total_price += $cart->price;
                    ?>
                    <tr>

                        <td><a href="product_detail.php?id=<?= $cart->id;?>"><img alt="" src="<?= $cart->image;?>"
                                    style="width: 100px;"></a>
                        </td>
                        <td>Fusce id molestie massa</td>
                        <td><input type="number" placeholder="1" class="input-mini"> </td>
                        <td><?= "$".$cart->price;?></td>
                        <td><a class="btn btn-danger" href="deletecart.php?id=<?=$cart->id;?>">delete</a></td>
                    </tr>
                    <?php endforeach; ?>

                </tbody>
            </table>
            <h4>What would you like to do next?</h4>
            <p>Choose if you have a discount code or reward points you want to use or would like to estimate your
                delivery cost.</p>
            <label class="radio">
                <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked="">
                Use Coupon Code
            </label>
            <label class="radio">
                <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
                Estimate Shipping &amp; Taxes
            </label>
            <hr>
            <p class="cart-total right">
                <strong>Total :</strong><?= "$".number_format($total_price);?><br>
            </p>
            <hr />
            <p class="buttons center">

                <a class="btn" type="button" href="index.php">Update</a>
                <a class="btn" type="button" href="index.php">Continue</a>
                <a class="btn btn-inverse" type="button" href="index.php"> Buying </a>
                <!-- <a class="btn btn-inverse" type="button" href="index.php"> Checkout </a> -->

            </p>
        </div>
        <div class="span3 col">
            <div class="block">
                <ul class="nav nav-list">
                    <li class="nav-header">SUB CATEGORIES</li>
                    <li><a href="products.php">Nullam semper elementum</a></li>
                    <li class="active"><a href="products.php">Phasellus ultricies</a></li>
                    <li><a href="products.php">Donec laoreet dui</a></li>
                    <li><a href="products.php">Nullam semper elementum</a></li>
                    <li><a href="products.php">Phasellus ultricies</a></li>
                    <li><a href="products.php">Donec laoreet dui</a></li>
                </ul>
                <br />
                <ul class="nav nav-list below">
                    <li class="nav-header">MANUFACTURES</li>
                    <li><a href="products.php">Adidas</a></li>
                    <li><a href="products.php">Nike</a></li>
                    <li><a href="products.php">Dunlop</a></li>
                    <li><a href="products.php">Yamaha</a></li>
                </ul>
            </div>
            <div class="block">
                <h4 class="title">
                    <span class="pull-left"><span class="text">Randomize</span></span>
                    <span class="pull-right">
                        <a class="left button" href="#myCarousel" data-slide="prev"></a><a class="right button"
                            href="#myCarousel" data-slide="next"></a>
                    </span>
                </h4>
                <div id="myCarousel" class="carousel slide">
                    <div class="carousel-inner">
                        <div class="active item">
                            <ul class="thumbnails listing-products">
                                <li class="span3">
                                    <div class="product-box">
                                        <span class="sale_tag"></span>
                                        <a href="product_detail.php"><img alt=""
                                                src="themes/images/ladies/03.jpg"></a><br />
                                        <a href="product_detail.php" class="title">Fusce id molestie massa</a><br />
                                        <a href="#" class="category">Suspendisse aliquet</a>
                                        <p class="price">$200061</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="item">
                            <ul class="thumbnails listing-products">
                                <li class="span3">
                                    <div class="product-box">
                                        <a href="product_detail.php"><img alt=""
                                                src="themes/images/ladies/04.jpg"></a><br />
                                        <a href="product_detail.php" class="title">Tempor sem sodales</a><br />
                                        <a href="#" class="category">Urna nec lectus mollis</a>
                                        <p class="price">$134000</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include "layouts/footer.php"; ?>