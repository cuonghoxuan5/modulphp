<?php
    session_start();
    include_once "../bills/db.php";
    $user   = "SELECT * FROM customers";
    $stmt   =$connect->query($user);
    $stmt->setFetchMode(PDO::FETCH_OBJ);
    $rows   =$stmt->fetchAll();
    
?>
<?php if (isset($_SESSION['error'])) : ?>
        <div class="alert alert-danger">
            <?= $_SESSION['error']; unset($_SESSION['error']); ?>
        </div>
 <?php endif; ?>
<?php if (isset($_SESSION['success'])) : ?>
    <div class="alert alert-success">
        <?= $_SESSION['success'];unset($_SESSION['success']); ?>
    </div>
<?php endif; ?>

<?php  include_once "../bills/header.php"; ?>
<div class="container">
    <table class="table">
        <h1 class="text-center text-warning"> Customers </h1>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="../bills/user.php">Bills</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="../categories/category.php"> Categories <span
                                class="sr-only"></span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../oder_dentails/order.php">Oder_Dentails</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="../products/product.php">Products</a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>
            </div>
        </nav>
        <hr>
        <thead>
            <tr>
                <th scope="col"> STT </th>
                <th scope="col"> Name</th>
                <th scope="col"> Address</th>
                <th scope="col"> phone</th>
                <th scope="col"> password</th>
                <th scope="col"> account</th>
                <th scope="col"> Action</th>
            </tr>
        </thead>
        <?php foreach ($rows as $key => $customers): ?>
        <tr>
            <td>
                <?= $key++ ; ?>
            </td>
            <td>
                <?= $customers->name ; ?>
            </td>
            <td>
                <?= $customers->address ; ?>
            </td>
            <td>
                <?= $customers->phone; ?>
            </td>
            <td>
                <?= $customers->password;?>
            </td>
            <td>
                <?= $customers->account; ?>
            </td>
            <td>
                <a class="btn btn-primary" href="edit.php?id=<?= $customers->id;?>"> Edit </a>
                <a class="btn btn-danger" href="delete.php"> Delete </a>
                <a class="btn btn-info" href="create.php"> Create </a>
            </td>
        </tr>
        <?php endforeach; ?>