<?php
    session_start();
    include_once "../bills/db.php";
    $user   = "SELECT products.*,categories.name as category_name FROM `products`
    JOIN categories ON products.category_id = categories.id";
    // $stmt   =$connect->query($user);
    // $stmt->setFetchMode(PDO::FETCH_OBJ);
    // $rows   =$stmt->fetchAll();
    $edit = $connect->query($user);
    $edit->setFetchMode(PDO::FETCH_OBJ);
    $rows = $edit->fetchAll(); 
    // echo "<pre>";
    // print_r($rows);
    // die();
?>

<?php  include_once "../bills/header.php"; ?>
<div class="container">
    <table class="table">
        <h1 class="text-center text-warning"> Products </h1>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="../bills/user.php">Bills</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="../categories/category.php"> Categories <span
                                class="sr-only"></span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../customers/customer.php">Customers</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="../oder_dentails/order.php">Order_Dentails</a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>
            </div>
        </nav>
        <hr>
        <thead>
            <tr>
                <th scope="col"> STT </th>
                <th scope="col"> Name </th>
                <th scope="col"> Color </th>
                <th scope="col"> Image </th>
                <th scope="col"> Category_Name </th>
                <th scope="col"> Price </th>
                <th scope="col"> Action </th>
            </tr>
        </thead>
        <?php foreach ($rows as $key => $product): ?>
        <tr>
            <td>
                <?= $key++ ; ?>
            </td>
            <td>
                <?= $product->name ; ?>
            </td>
            <td>
                <?= $product->color ; ?>
            </td>
            <td>
                <img width="150px" height="80px" src=" <?= $product->image; ?>" alt="">
            </td>
            <td>
                <?= $product->category_name;?>
            </td>
            <td>
                <?php echo number_format($product->price,0,',','.').'$'?>
            </td>
            <td>
                <a class="btn btn-primary" href="edit.php?id=<?= $product->id;?>"> Edit </a>
                <a class="btn btn-danger" href="delete.php?id=<?= $product->id;?>"> Delete </a>
                <a class="btn btn-info" href="create.php"> Create </a>
            </td>
        </tr>
        <?php endforeach; ?>