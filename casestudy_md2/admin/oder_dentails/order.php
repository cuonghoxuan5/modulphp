<?php
    session_start();
    include_once "../bills/db.php";
    $user   = "SELECT * FROM oder_dentails";
    // $stmt   =$connect->query($user);
    // $stmt->setFetchMode(PDO::FETCH_OBJ);
    // $rows   =$stmt->fetchAll();
    $edit = $connect->query($user);
    $edit->setFetchMode(PDO::FETCH_OBJ);
    $rows = $edit->fetchAll(); 
    
?>

<?php  include_once "../bills/header.php"; ?>
<div class="container">
    <table class="table">
        <h1 class="text-center text-warning"> Order Details </h1>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="../bills/user.php">Bills</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="../categories/category.php"> Categories <span
                                class="sr-only"></span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../customers/customer.php">Customers</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="../products/product.php">Products</a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>
            </div>
        </nav>
        <hr>

        <thead>
            <tr>
                <th scope="col"> STT </th>
                <th scope="col"> Bill_id </th>
                <th scope="col"> Product_id </th>
                <th scope="col"> Date </th>
                <th scope="col"> Total Money</th>
                <th scope="col"> Quantity </th>
                <th scope="col"> Action </th>
            </tr>
        </thead>
        <?php foreach ($rows as $key => $oder_dentail): ?>
        <tr>
            <td>
                <?= $key++ ; ?>
            </td>
            <td>
                <?= $oder_dentail->bill_id ; ?>
            </td>
            <td>
                <?= $oder_dentail->product_id ; ?>
            </td>
            <td>
                <?= $oder_dentail->date_order; ?>
            </td>
            <td>
                <?= $oder_dentail->total_money;?>
            </td>
            <td>
                <?= $oder_dentail->quantity; ?>
            </td>
            <td>
                <a class="btn btn-primary" href="edit.php?id=<?= $oder_dentail->id;?>"> Edit </a>
                <a class="btn btn-danger" href="delete.php?id=<?= $oder_dentail->id;?>"> Delete </a>
                <a class="btn btn-info" href="create.php"> Create </a>
            </td>
        </tr>
        <?php endforeach; ?>