<?php 
    session_start();
    include "db.php";
    $sql    = "SELECT * FROM bills";
    //thực hiện truy vấn
    $stmt  = $connect->query( $sql );
    //tùy chọn kiểu trả về
    $stmt->setFetchMode(PDO::FETCH_OBJ);
    //lấy tất cả kết quả
    $rows   = $stmt->fetchAll();
    
?>

<?php if (isset($_SESSION['error'])) : ?>
<div class="alert alert-danger">
    <?= $_SESSION['error'];
            unset($_SESSION['error']); ?>
</div>
<?php endif; ?>

<?php if (isset($_SESSION['success'])) : ?>
<div class="alert alert-success">
    <?= $_SESSION['success'];
            unset($_SESSION['success']); ?>
</div>
<?php endif; ?>


<?php include_once "header.php"; ?>

<div class="container">
    <table class="table table-striped">
        <h1 class="text-center text-warning"> Order <h1>
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <a class="navbar-brand" href="../categories/category.php">Categoies</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" style="font-size:21px;" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link" href="../customers/customer.php"> Customers <span
                                        class="sr-only"></span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="../oder_dentails/order.php">Oder_Dentails</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link " href="../products/product.php">Products</a>
                            </li>
                        </ul>
                        <form class="form-inline my-2 my-lg-0">
                            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                        </form>
                    </div>
                </nav>
                <hr>
                <thead>

                    <tr>
                        <th scope="col">Stt</th>
                        <th scope="col">Customer_id</th>
                        <th scope="col">Price</th>
                        <th scope="col">Date</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <?php foreach ($rows as $key =>$bills):
            // echo "<pre>"; 
            // print_r($bills);
            // die();
            ?>

                <tr>
                    <td>
                        <?= $key++; ?>
                    </td>
                    <td>
                        <?= $bills->customer_id; ?>
                    </td>
                    <td>
                        <?= $bills->price; ?>
                    </td>
                    <td>
                        <?= $bills->date; ?>
                    </td>
                    <td>
                        <a class="btn btn-primary" href="edit.php?id=<?php echo $bills->id ;?>">Edit</a>
                        <a class="btn btn-danger" href="delete.php?id=<?php echo $bills->id ;?>">Delete</a>
                        <a class="btn btn-info" href="create.php">Create</a>
                    </td>

                </tr>
                <?php endforeach; ?>


                <!-- <tbody>
            <tr>
            <th scope="row">1</th>
            <td>Mark</td>
            <td>Otto</td>
            <td>@mdo</td>
            <td>@mdo</td>
            </tr>
            <tr>
            <th scope="row">2</th>
            <td>Jacob</td>
            <td>Thornton</td>
            <td>@fat</td>
            <td>@fat</td>
            </tr>
            <tr>
            <th scope="row">3</th>
            <td >Larry the Bird</td>
            <td >Larry the Bird</td>
            <td >Larry the Bird</td>
            <td>@twitter</td>
            </tr>
            <tr>
            <th scope="row">4</th>
            <td >Larry the Bird</td>
            <td >Larry the Bird</td>
            <td >Larry the Bird</td>
            <td>@twitter</td>
            </tr>
        </tbody> -->
    </table>
</div>