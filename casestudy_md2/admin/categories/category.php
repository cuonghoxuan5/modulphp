<?php
   
    include_once "../bills/db.php";
    $user   = "SELECT * FROM categories";
    $stmt   =$connect->query($user);
    $stmt->setFetchMode(PDO::FETCH_OBJ);
    $rows   =$stmt->fetchAll();
    
?>
<?php
    session_start();
    if (isset($_SESSION['error'])) : ?>
<div class="alert alert-danger">
    <?= $_SESSION['error'];
            unset($_SESSION['error']); ?>
</div>
<?php endif; ?>
<?php if (isset($_SESSION['success'])) : ?>
<div class="alert alert-success">
    <?= $_SESSION['success'];
            unset($_SESSION['success']); ?>
</div>
<?php endif; 
?>

<?php  include_once "../bills/header.php"; ?>
<div class="container">
    <table class="table">
        <h1 class="text-center text-warning"> Categories </h1>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="../bills/user.php">Bills</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="../customers/customer.php"> Customers <span
                                class="sr-only"></span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="../oder_dentails/order.php">Oder_Dentails</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="../products/product.php">Products</a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>
            </div>
        </nav>
        <hr>
        <thead>
            <tr>
                <th scope="col">STT </th>
                <th scope="col">Name</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <?php foreach($rows as $key => $categories):?>
        <tr>
            <td>
                <?= $key++ ; ?>
            </td>
            <td>
                <?= $categories->name ; ?>
            </td>
            <td>
                <a href="edit.php?id=<?= $categories->id ;?>" class="btn btn-primary">Edit</a>
                <a href="create.php" class="btn btn-info">Create</a>
                <a href="delete.php" class="btn btn-danger">Delete</a>
            </td>
        </tr>
        <?php endforeach; ?>
    </table>
</div>