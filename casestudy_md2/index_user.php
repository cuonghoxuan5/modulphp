<?php         session_start();
	 if (!isset($_SESSION)) {

    }
	include_once "admin/database.php";
	include_once "layouts/header_user.php";
	
	// include_once "admin/login.php";

	$sql = "SELECT * FROM products";
	$stmt = $connect->query($sql);
	$stmt->setFetchMode(PDO::FETCH_OBJ);
	$rows	=$stmt->fetchAll();

?>

<section class="main-content">
    <div class="row">
        <div class="span12">
            <div class="row">
                <div class="span12">
                    <h4 class="title">
                        <span class="pull-left"><span class="text"><span class="line">Feature
                                    <strong>Products</strong></span></span></span>
                        <span class="pull-right">
                            <a class="left button" href="#myCarousel" data-slide="prev"></a><a class="right button"
                                href="#myCarousel" data-slide="next"></a>
                        </span>
                    </h4>

                    <div id="myCarousel" class="myCarousel carousel slide">
                        <div class="carousel-inner">
                            <div class="active item">
                                <ul class="thumbnails">
                                    <?php foreach($rows as $product):?>
                                    <li class="span3">
                                        <div class="product-box">
                                            <span class="sale_tag"></span>
                                            <p><a href="product_detail.php?id=<?= $product->id;?>"> <img
                                                        src="<?= $product->image;?>" style="width:240px; height: 160px;"
                                                        alt="" /></a></p>
                                            <a href="product_detail.php" class="title"><?= $product->name;?></a><br />
                                            <a href="products.php" class="category">Commodo consequat</a>
                                            <p class="price"><?= "$".$product->price;?></p>
                                        </div>
                                    </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row feature_box">
                <div class="span4">
                    <div class="service">
                        <div class="responsive">
                            <img src="themes/images/feature_img_2.png" alt="" />
                            <h4>MODERN <strong>DESIGN</strong></h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and printing industry unknown printer.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="service">
                        <div class="customize">
                            <img src="themes/images/feature_img_1.png" alt="" />
                            <h4>FREE <strong>SHOPPING</strong></h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and printing industry unknown printer.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="service">
                        <div class="support">
                            <img src="themes/images/feature_img_3.png" alt="" />
                            <h4>24/7 LIVE <strong>SUPPORT</strong></h4>
                            <p>Lorem Ipsum is simply dummy text of the printing and printing industry unknown printer.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="our_client">
    <h4 class="title"><span class="text">Manufactures</span></h4>
    <div class="row">
        <div class="span2">
            <a href="#"><img alt="" src="themes/images/clients/1.jpg"></a>
        </div>
        <div class="span2">
            <a href="#"><img alt="" src="themes/images/clients/2.jpg"></a>
        </div>
        <div class="span2">
            <a href="#"><img alt="" src="themes/images/clients/3.jpg"></a>
        </div>
        <div class="span2">
            <a href="#"><img alt="" src="themes/images/clients/4.jpg"></a>
        </div>
        <div class="span2">
            <a href="#"><img alt="" src="themes/images/clients/5.jpg"></a>
        </div>
        <div class="span2">
            <a href="#"><img alt="" src="themes/images/clients/6.jpg" style="width:800px ; height:110px" alt="" /></a>
            </p>
        </div>
    </div>
</section>
<?php include_once "layouts/footer.php"; ?>